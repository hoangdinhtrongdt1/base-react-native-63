import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AuthStack from './AuthStack';
import AppStack from './AppStack';
import {useSelector} from 'react-redux';
import SplashScreen from '../screens/SplashScreen';

const Stack = createStackNavigator();

const _renderItemStack = (name: string, component: any) => (
  <Stack.Screen name={name} component={component} />
);

const AppNavigation = () => {
  const {isSplash, isLogin} = useSelector((state: any) => state.auth);
  // uncomment if app have wellcomescreen
  // const {isComplete} = useSelector((state: any) => state.welcome);
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        {isSplash && _renderItemStack('SplashScreen', SplashScreen)}
        {/* uncomment if app have wellcomescreen */}
        {/* {!isComplete && _renderItemStack('WelcomeStack', WelcomeStack)} */}
        {isLogin && _renderItemStack('AuthStack', AuthStack)}
        {_renderItemStack('AppStack', AppStack)}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigation;
