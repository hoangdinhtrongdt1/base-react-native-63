import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';

const Stack = createStackNavigator();

const _renderItemStack = (name: string, component: any) => (
  <Stack.Screen name={name} component={component} />
);

const AppStack = () => (
  <Stack.Navigator headerMode="none">
    {_renderItemStack('HomeScreen', HomeScreen)}
  </Stack.Navigator>
);

export default AppStack;
