import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen';

const Stack = createStackNavigator();

const _renderItemStack = (name: string, component: any) => (
  <Stack.Screen name={name} component={component} />
);

const AuthStack = () => (
  <Stack.Navigator headerMode="none">
    {_renderItemStack('LoginScreen', LoginScreen)}
  </Stack.Navigator>
);

export default AuthStack;
