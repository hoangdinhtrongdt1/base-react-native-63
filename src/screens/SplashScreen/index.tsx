import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import styles from './style';

interface Props {
  navigation: any;
}
const SplashScreen = (props: Props) => {
  const {navigation} = props;
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('AuthStack');
    }, 2000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.container}>
      <Text>SplashScreen</Text>
    </View>
  );
};
export default SplashScreen;
