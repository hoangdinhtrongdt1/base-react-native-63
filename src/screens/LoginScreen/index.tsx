import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import styles from './style';
import {setSplash} from '../../redux/actions/auth';
import {useDispatch} from 'react-redux';

interface Props {
  navigation: any;
}
const LoginScreen = (props: Props) => {
  const {navigation} = props;
  const dispatch = useDispatch();

  useEffect(() => {
    _removeStack();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const _removeStack = async () => {
    // loại bỏ màn splash khỏi stack
    await dispatch(setSplash(false));
  };

  const _onPressLogin = () => {
    navigation.navigate('AppStack');
  };

  return (
    <View style={styles.container}>
      <Text onPress={_onPressLogin}>Login</Text>
    </View>
  );
};
export default LoginScreen;
